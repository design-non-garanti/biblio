00:04
we drew inspiration from paper and ink
00:08
however unlike real paper arm digital
00:12
material can expand reform and reshape
00:15
intelligently material has physical
00:18
surfaces and edges because the human
00:20
want mind is wired at its most primitive
00:23
level to instinctively understand
00:25
objects and their relationships those
00:27
scenes and shadows provide meaning about
00:30
what you can touch and how it will move
00:32
in the real world every small change in
00:36
position and depth creates subtle but
00:39
important changes in lighting and
00:41
shadows so as part of the L preview will
00:44
now allow app developers to specify an
00:47
elevation value for any UI surface and
00:50
the framework will render it in correct
00:53
perspective with virtual light sources
00:55
and real-time shadows
01:02
material design is beautiful and bold
01:05
because clean typographic layouts are
01:07
simple and easy to understand your
01:10
content is the focus so the L preview
01:13
will allow app developers to easily
01:15
colorize all framework elements in your
01:18
app to match the theme to your brand and
01:20
we're previewing a new support library
01:23
that we call palette to easily extract
01:25
colors from images and really put those
01:27
vivid pictures front and center we're
01:31
giving designers familiar tools like
01:34
baseline grids that work across screens
01:36
grids and sure apps have a consistent
01:39
rhythm and character and this will allow
01:41
you to start with a design on a phone
01:43
and logically and easily bring that same
01:46
design to tablets and laptops now what
01:50
design doesn't mean one size fits all
01:52
our guidelines allow you to
01:54
appropriately adapt the UI so your users
01:58
will already know their way around your
01:59
app no matter what screen they use it on
02:03
and we've also updated our system font
02:07
Roboto
02:08
so that designers and developers can use
02:10
one typeface designed and optimized for
02:13
every screen from your watch to your
02:16
laptop to your television so now let's
02:19
talk about animation it's delightful
02:22
when your touch is rewarded with motion
02:24
and material surfaces slide around if
02:27
the physics of cardstock but they
02:29
respond to touch with splashes of
02:31
virtual ink that are like ripples in a
02:33
pond as part of the L preview all of
02:36
your applications UI building blocks
02:38
have been updated to include rich
02:41
animated touch feedback
02:45
and no detail is too small to bring a
02:48
smile to your face like when the reload
02:51
button loops around or the playback
02:53
controls can change finally in the real
02:56
world
02:56
nothing teleports from one place to
02:59
another and that's why it's so important
03:01
to animate every change on screen in a
03:04
way that makes sense in the L preview
03:06
Android developers will now be able to
03:09
create seamless animations from any
03:11
screen to any other between activities
03:14
and even between apps so you're probably
03:21
wondering how this looks like in
03:23
practice we're gonna give you a sneak
03:25
peek at one of our familiar google
03:26
applications in the new material design
03:31
here you can see step by step how we
03:34
update the design the typography the
03:40
grid changes and finally the surfaces
03:46
and bold colors and a few small changes
03:49
make a really big difference and you can
03:53
also see how easy it is to take that
03:54
same design to different screens now
04:00
I've talked about only a few of the
04:02
highlights of material design and just
04:04
some of the api's that you can try out
04:07
in the Android L preview but as we all
04:09
know people spend an enormous amount of
04:12
time on the web and especially the
04:14
mobile web last year at i/o we announced
04:17
polymer which is a powerful new UI
04:20
library for the web today we're bringing
04:23
you all of the material design
04:25
capabilities to the web through polymer
04:29
as a web developer you'll be able to
04:32
build applications out of material
04:35
design building blocks with all of the
04:37
same surfaces both graphics and smooth
04:40
animations at 60 frames per second
04:47
so between the L preview and polymer you
04:51
can bring the same rich fluid material
04:54
design to every screen and to help you
04:58
take full advantage of this framework
05:00
we've also completely redesigned and
05:02
created one unified set of style
05:05
guidelines for every screen and for all
05:08
devices these guidelines will help
05:10
designers and developers alike
05:12
understand best practices and build
05:14
consistent beautiful experiences we're
05:17
releasing the first draft of these
05:19
guidelines as part of our preview today
05:21
at google.com slash design
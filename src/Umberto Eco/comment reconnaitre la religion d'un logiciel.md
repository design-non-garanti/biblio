Comment reconnaître la religion d'un logiciel ? 

Une nouvelle guerre de religions modifie subrepticement notre monde contemporain. J'en suis convaincu depuis longtemps, et lorsque j'évoque cette idée, je m'aperçois qu'elle recueille aussitôt un consensus. 

Ceci n'a pu vous échapper, le monde est aujourd'hui divisé en deux : d'un côté les partisans du Macintosh, de l'autre ceux du PC sous Ms-Dos. Eh bien, je suis intimement persuadé que le Mac est catholique et le Dos protestant. Je dirais même plus. 

Le Mac est catholique contre-réformateur, empreint de la « ratio studiorum » des jésuites. Il est convivial, amical, conciliant, il explique pas à pas au fidèle la marche à suivre pour atteindre, sinon le royaume des cieux, du moins l'instant final de l'impression du document. Il est catéchistique, l'essence de la révélation est résolue en formules compréhensibles et en icônes somptueuses. Tout le monde a droit au salut. 

Le Dos est protestant, voire carrément calviniste. Il prévoit une libre interprétation des Écritures, requiert des décisions tourmentées, impose une herméneutique subtile, garantit que le salut n'est pas à la portée de tous. Faire marcher le système nécessite un ensemble d'actes personnels interprétatifs du logiciel : seul, loin de la communauté baroque des joyeux drilles, l'utilisateur est enfermé dans son obsession intérieure. 

On m'objectera que l'arrivée de Windows a rapproché l'univers du Dos de la tolérance contre-réformatrice du Mac. Rien de plus exact. Windows constitue un schisme de type anglican, de somptueuses cérémonies au sein des cathédrales, mais toujours la possibilité de revenir au Dos afin de modifier un tas de choses en se fondant sur d'étranges décisions : tout compte fait, les femmes et les gays pourront accéder au sacerdoce1. 

Naturellement, catholicisme et protestantisme des deux systèmes n'ont rien à voir avec les positions culturelles et religieuses des usagers. J'ai découvert l'autre jour que Franco Fortini, poète sévère et tourmenté, ennemi déclaré de la société du spectacle, est un adepte du Mac. Cela dit, il est légitime de se demander si à la longue, au fil du temps, l'emploi d'un système plutôt que d'un autre ne cause pas de profondes modifications intérieures. Peut-on vraiment être à la fois adepte du Dos et catholique traditionaliste ? Par ailleurs, Céline aurait-il écrit avec Word, Wordperfect ou Wordstar ? Enfin, Descartes aurait-il programmé en Pascal ? 

Et le langage machine, qui décide de notre destin en sous-main et pour n'importe quel environnement ? Eh bien, cela relève de l'Ancien Testament, du Talmud et de la Cabale. Ah, encore et toujours le lobby juif. (1994) 

1. Évidemment, Windows 95 — résolument anglo-catholique — vient compliquer aujourd'hui ce panorama théologique
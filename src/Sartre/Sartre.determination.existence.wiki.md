#L'existence précède l'essence

Dans la conférence intitulée L'existentialisme est un humanisme, du 29 octobre 1945, Sartre développe l'idée que l'homme n'ayant pas de nature définie a priori, il est libre de se définir lui-même par son projet. « Qu'est-ce que signifie ici que l'existence précède l'essence ? Cela signifie que l'homme existe d'abord, se rencontre, surgit dans le monde, et qu'il se définit après »61.

Sartre rattache la liberté de l'homme au fait que Dieu n'existe pas, reprenant en un sens positif la phrase de Dostoïevski, « Si Dieu n'existe pas, tout est permis ». Il prend cette formule au sérieux : « il n'y a pas de nature humaine, puisqu'il n'y a pas de Dieu pour la concevoir ». L'homme n'est pas de toute éternité, dans l'esprit d'un Dieu créateur, comme l'idée d'un objet technique (tel un coupe-papier) dans l'esprit de l'artisan. Par conséquent, aucune norme transcendante n'indique à l'homme ce qu'il doit faire. L'homme est libre, « il est liberté », et n'est rien d'autre que ce qu'il se fait.

Sartre explique que cette liberté implique une responsabilité : en se choisissant lui-même, l'homme établit un modèle de ce qui vaut pour l'homme en général. « Ainsi, notre responsabilité est beaucoup plus grande que nous ne pourrions le supposer, car elle engage l'humanité entière »62. En faisant de chacun « un législateur qui choisit pour l'humanité entière », Sartre retrouve aussitôt l'universel, dont il semblait s'écarter en confrontant l'individu à la liberté absolue de son choix, sur fond d'« angoisse » et de « délaissement », deux concepts inspirés, de la lecture de Kierkegaard et de Heidegger. On ne peut échapper ni à la liberté du choix de son existence et de ses actions, ni à leur caractère exemplaire pour tout homme : l'invocation de motifs pour ne pas exercer sa liberté est assimilée à de la « mauvaise foi ».

Certaines formules de L'existentialisme est un humanisme sont restées célèbres, comme « Nous sommes seuls, sans excuses », ou bien « L'homme est condamné à être libre », qui fait écho à son provocateur « nous n’avons jamais été aussi libres que sous l’Occupation », publié en septembre 1944 dans les Lettres françaises63.

#Liberté et aliénation

Selon Sartre, l'homme est ainsi libre de choisir son essence. Pour lui, contrairement à Hegel, il n'y a pas d'essence déterminée, l'essence est librement choisie par l'existant. L'Homme est absolument libre, il n'est rien d'autre que ce qu'il fait de sa vie, il est un projet. Sartre nomme ce dépassement d'une situation présente par un projet à venir, la transcendance.

L'existentialisme de Sartre s'oppose ainsi au déterminisme qui stipule que l'homme est le jouet de circonstances dont il n'est pas maître. Sartre estime que l'homme choisit parmi les événements de sa vie, les circonstances qu'il décidera déterminantes. Autrement dit, il a le pouvoir de 'néantiser', c'est-à-dire de combattre les déterminismes qui s'opposent à lui.

Au nom de la liberté de la conscience, Sartre refuse le concept freudien d'inconscient remplacé par la notion de « mauvaise foi » de la conscience. L'Homme ne serait pas le jouet de son inconscient mais choisirait librement de se laisser nouer par tel ou tel traumatisme. Ainsi, l'inconscient ne saurait amoindrir l'absolue liberté de l'Homme.

Selon Sartre, l'homme est condamné à être libre. L'engagement n'est pas une manière de se rendre indispensable mais responsable. Ne pas s'engager est encore une forme d'engagement.

L'existentialisme de Sartre est athée, c'est-à-dire que, pour lui, Dieu n'existe pas (ou en tout cas « s'Il existait cela ne changerait rien »), donc l'homme est seul source de valeur et de moralité ; il est condamné à inventer sa propre morale et libre de la définir. Le critère de la morale ne se trouve pas au niveau des « maximes » (Kant) mais des « actes ». La « mauvaise foi », sur un plan pratique, consiste à dire : « c'est l'intention qui compte ».

Selon Sartre, la seule aliénation à cette liberté de l'homme est la volonté d'autrui. Ainsi fait-il dire à Garcin dans Huis clos « L'Enfer c'est les Autres ».

https://fr.wikipedia.org/wiki/Jean-Paul_Sartre
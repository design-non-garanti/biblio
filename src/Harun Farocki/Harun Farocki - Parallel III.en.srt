1
00:00:09,980 --> 00:00:15,110
we can drop right into<font color="#CCCCCC"> the middle</font><font color="#E5E5E5"> of</font>

2
00:00:12,630 --> 00:00:15,110
<font color="#E5E5E5">events</font>

3
00:00:25,770 --> 00:00:32,539
and with a simple<font color="#CCCCCC"> command we can</font><font color="#E5E5E5"> just as</font>

4
00:00:29,369 --> 00:00:32,539
easily leave the place

5
00:00:57,920 --> 00:01:03,870
before long<font color="#E5E5E5"> the limit of the game</font>

6
00:01:00,630 --> 00:01:07,970
surface is reached<font color="#E5E5E5"> beyond the surface is</font>

7
00:01:03,870 --> 00:01:07,970
a backdrop<font color="#CCCCCC"> as in a theater</font>

8
00:01:24,980 --> 00:01:28,099
[Music]

9
00:01:30,430 --> 00:01:34,740
the world ends like a<font color="#CCCCCC"> boardgame</font>

10
00:01:41,659 --> 00:01:49,549
the two men crashed through<font color="#CCCCCC"> the hedge</font>

11
00:01:43,789 --> 00:01:51,610
<font color="#CCCCCC">and not a single leaf</font><font color="#E5E5E5"> flutters the</font>

12
00:01:49,549 --> 00:01:54,530
objects have no<font color="#E5E5E5"> existence besides</font>

13
00:01:51,610 --> 00:02:01,670
<font color="#CCCCCC">themselves they are</font><font color="#E5E5E5"> by themselves</font>

14
00:01:54,530 --> 00:02:09,080
<font color="#E5E5E5">nothing each of their characteristics</font>

15
00:02:01,670 --> 00:02:11,120
<font color="#CCCCCC">must</font><font color="#E5E5E5"> be specially constructed here is</font>

16
00:02:09,080 --> 00:02:14,830
the construction<font color="#E5E5E5"> of branches set in</font>

17
00:02:11,120 --> 00:02:14,830
movement<font color="#CCCCCC"> by the</font><font color="#E5E5E5"> game character</font>

18
00:02:43,550 --> 00:02:49,850
here the camera<font color="#CCCCCC"> tries to penetrate the</font>

19
00:02:46,380 --> 00:02:49,850
pedestal of<font color="#E5E5E5"> a public sculpture</font>

20
00:02:53,430 --> 00:03:01,740
pedastal body is constructed<font color="#CCCCCC"> like a</font>

21
00:02:56,230 --> 00:03:01,740
<font color="#E5E5E5">stern the</font><font color="#CCCCCC"> bullets smash craters into it</font>

22
00:03:05,760 --> 00:03:30,910
in the special<font color="#E5E5E5"> theater mode the camera</font>

23
00:03:09,340 --> 00:03:32,710
<font color="#E5E5E5">can penetrate the base like the camera</font>

24
00:03:30,910 --> 00:03:35,410
in the<font color="#E5E5E5"> feature film that dollies through</font>

25
00:03:32,710 --> 00:03:37,800
walls<font color="#E5E5E5"> to establish the narrator's</font>

26
00:03:35,410 --> 00:03:37,800
omnipotence

27
00:03:54,290 --> 00:03:59,150
it is evident that<font color="#E5E5E5"> the block is hollow</font>

28
00:04:00,830 --> 00:04:05,990
the walls<font color="#E5E5E5"> are invisible from</font><font color="#CCCCCC"> the inside</font>

29
00:04:21,620 --> 00:04:28,590
here the camera<font color="#E5E5E5"> tries to</font><font color="#CCCCCC"> penetrate the</font>

30
00:04:24,390 --> 00:04:34,980
<font color="#CCCCCC">ground it glances off like a</font><font color="#E5E5E5"> shovel</font>

31
00:04:28,590 --> 00:04:38,610
striking<font color="#E5E5E5"> hard earth the surface of the</font>

32
00:04:34,980 --> 00:04:40,919
<font color="#CCCCCC">earth is invisible from</font><font color="#E5E5E5"> below it lacks</font>

33
00:04:38,610 --> 00:04:43,490
the characteristic of being<font color="#E5E5E5"> visible from</font>

34
00:04:40,919 --> 00:04:43,490
<font color="#E5E5E5">both sides</font>

35
00:05:29,910 --> 00:05:38,099
the camera can easily penetrate<font color="#CCCCCC"> the</font>

36
00:05:32,680 --> 00:05:55,599
rocky<font color="#E5E5E5"> cliff the rock is not massive</font>

37
00:05:38,099 --> 00:06:04,840
<font color="#E5E5E5">under it it's the sea the surface of the</font>

38
00:05:55,599 --> 00:06:09,750
water is<font color="#E5E5E5"> nothing but surface there is</font><font color="#CCCCCC"> no</font>

39
00:06:04,840 --> 00:06:09,750
water below it it floats<font color="#E5E5E5"> in emptiness</font>

40
00:06:11,490 --> 00:06:17,610
this<font color="#CCCCCC"> world's loads like an island in the</font>

41
00:06:14,500 --> 00:06:17,610
<font color="#E5E5E5">primeval ocean</font>

42
00:07:12,300 --> 00:07:14,360
you


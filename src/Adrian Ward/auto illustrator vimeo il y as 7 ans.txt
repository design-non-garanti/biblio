https://vimeo.com/17593822
This is both an archive of a software art piece and a software demonstration. This was made for the MU (Eindhoven) leg of the Funware exhibition. More details below:

Signwave Auto-illustrator is a piece of software and an artwork in its own right written by Arian Ward, which parodies the well established vector graphics application by Adobe.

Auto-illustrator is a fully functioning vector graphics application that on the surface (GUI) appears to be no different from the proprietary or FLOSS alternatives such as Illustrator or Inkscape (respectively). However the difference appears when the software, during use, transfers a great deal of control and creative decision making from the user to the software algorithms. The software is partially generative and overtly semi-autonomous.

It questions the control that we have when working with these types of proprietary ‘creative suites’, in which we have no access to study or modify the algorithms which define the ‘paint brush’ or the ‘pen’ tool. When we draw with these tools we are working within the parameters defined by the authors of the algorithms and also within the lineage of an inadequate canvas and paintbrush metaphor. Auto-illustrator both brings these facts to the surface and offers and alternative to software as a tool but instead as a collaborative creative experience that involves both the user and software with agency.

At the Transmediale.01 festival for art and digital culture, Auto-illustrator was given what is considered to be the first award soley dedicated to software art. The artwork is currently being revived at an exhibition, curated by Olga Goriunova (the curator of the software art repository RunMe.org), called Fun with Software which is currently at the Arnolfi gallery in Bristol and will continue to MU in Eindhoven.

Fun with Software looks at the history of software, and its relation to humour and fun:

"Making and using software can be experimental, humorous, and eventful. Alongside today’s rather dull use of forms, databases, schedules and processors, an element of fun has informed and guided the development of software from its beginnings. A good example of this is Love Letter Generator, conceived in the 1950s by one of the first programmers, Christopher Strachey, working with Alan Turing at Manchester University on one of the first computers, and reconstructed in this exhibition by David Link. This exhibition follows the development of software over the last fifty years through playful experimentation and art." – Statement from Arnolfi
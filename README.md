<meta charset="utf-8">

        │ │    ┌┬────┐         │ │    ┌┬────┐         │ │    ┌┐       
      │ ├─┘   ┌┘│    └┐      │ ├─┘   ┌┘│    └┐      │ ├─┘   ┌┘│          
    │ │ ├──  ┌┘┌┴┬──  └┐   │ │ ├──  ┌┘┌┴┬──  └┐   │ │ ├──  ┌┘┌┴┬──      
    │ ├─┘   ┌┘ │ ├─┐   └┐  │ ├─┘   ┌┘ │ ├─┐   └┐  │ ├─┘   ┌┘ │ ├─┐      
    └┬┴──  ┌┘  │ │ ├──  └┐ └┬┴──  ┌┘  │ │ ├──  └┐ └┬┴──  ┌┘  │ │ ├──     
     │    ┌┘     │ ├─┐   └┐ │    ┌┘     │ ├─┐   └┐ │    ┌┘     │ ├─┐    
     └────┘        │ │    └─┴────┘        │ │    └─┴────┘        │ │     

# Bibliothèque Design-non-garanti

## External ressources chamber

### retiary [link](http://www.retiary.org/)

> Laurie Spiegel
> Magic Mouse

### josephbasquin [link](https://josephbasquin.fr/bigpicturemobileversion)

> plenty of room

### typefaces.temporarystate [link](http://typefaces.temporarystate.net/preview/Panama)

> The copyright eye is watching

### Annarchive [link](https://annarchive.com/)

> WELCOME TO CYBERSPACE, VIDEO KIDS. YOU'VE FOUND ⒶNNARCHIVE,
> a permanent(?) collection of game media, archeaology, and artifacts for you to peruse.

### AHWA [link](http://rd.uqam.ca/AHWA/index.html)

> En 2003
> l'organisation
> AHWA-AWHA
> a été abolie.
> Mais ce site web survivra...!

### historia cultura [link](http://users.skynet.be/histcult/)

> Number of links to other Websites = 632 = today.
> Since it is constantly updated,
> this Website is always under construction.
> From these links you have access to
> more than 100.000 interesting Websites.
> Save your time and come back visiting !

### Treemaps for space-constrained visualization of hierarchies [link](https://www.cs.umd.edu/hcil/treemap/)

> Treempa history

### Paperrad [link](http://www.paperrad.org/)
 
> stylé

### Woodpecker hash Bruteforce [link](http://brute.zohosites.com/downloads.html)

> brute force for newbies

### paulbourke.net [link](http://paulbourke.net/)

> Data Formats, 3D, Audio, Image, Maths

### Folder Structure Conventions for games [link](https://github.com/Double-Fine-Game-Club/bad-golf-community-edition/wiki/folder-structure-and-naming-conventions)

### Folder Structure Conventions [link](https://github.com/kriasoft/Folder-Structure-Conventions)

### incident.net [link](http://incident.net/)

> chatonsky, julie morelle, vadim bernard

### teleferique.org [link](https://web.archive.org/web/20161012105853/http://www.teleferique.org/)

> collectif, démo, étienne cliquet

### ordigami.net [link](http://www.ordigami.net/pardefaut.html)

> ésthétique par défaut, étienne cliquet

### j_fenderson [link](https://twitter.com/j_fenderson)

> oscilloscope, music

### concours-spotify-premium-generateur-comptes-voles [link](https://www.klakinoumi.com/2018/04/24/concours-spotify-premium-generateur-comptes-voles/)

> python, bot, tweeter, followers, influencers, accounts, teen, mafia, lol

### zestedesavoir.com [link](zestedesavoir.com)

> tutoriels, amateur, libre
> site de tutos fondé par les anciens bénévoles du site du zéro dans le but de retrouver l'esprit bonenfant de leur ancienne plateforme, design du site élaboré par la communauté

### clofont.free.fr [link](http://clofont.free.fr/)

> web vernaculaire, page perso, typographie, kitsch, libre
> site d'une amatrice de la typo très kitsch et très fourni, très geek, [créé le 16 décembrre 1996

### theartofgooglebooks.tumblr.com [link](http://theartofgooglebooks.tumblr.com/)

> Child's autolinked writing-drawing.
> From the table of contents of Forget Me Not: A Gift for Sabbath School Children by Amanda M. Corey Edmond (1852). Original from Princeton University.
> Digitized February 15, 2008.

### asciipr0n.com [link](www.asciipr0n.com)

### simplifier.neocities.org [link](https://simplifier.neocities.org/)

### disnovation.org [link](http://disnovation.org/)

### anti-theory.com [link](http://www.anti-theory.com/soundart/circuitbend/)

## Internal ressources chamber

        │ │    ┌┬────┐         │ │    ┌┬────┐         │ │    ┌┐       
      │ ├─┘   ┌┘│    └┐      │ ├─┘   ┌┘│    └┐      │ ├─┘   ┌┘│          
    │ │ ├──  ┌┘┌┴┬──  └┐   │ │ ├──  ┌┘┌┴┬──  └┐   │ │ ├──  ┌┘┌┴┬──      
    │ ├─┘   ┌┘ │ ├─┐   └┐  │ ├─┘   ┌┘ │ ├─┐   └┐  │ ├─┘   ┌┘ │ ├─┐      
    └┬┴──  ┌┘  │ │ ├──  └┐ └┬┴──  ┌┘  │ │ ├──  └┐ └┬┴──  ┌┘  │ │ ├──     
     │    ┌┘     │ ├─┐   └┐ │    ┌┘     │ ├─┐   └┐ │    ┌┘     │ ├─┐    
     └────┘        │ │    └─┴────┘        │ │    └─┴────┘        │ │     
